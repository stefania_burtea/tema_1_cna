﻿using Grpc.Net.Client;
using GrpcServer;
using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Concurrent;
using Microsoft.Extensions.Logging;
using Grpc.Core;
using System.Collections.Generic;

namespace GrpcServer.Services
{
    public class CustomersService : Customer.CustomerBase
    {
        private readonly ILogger<CustomersService> _logger;
      
        public CustomersService(ILogger<CustomersService> logger)
        {
            _logger = logger;
        }
        public override Task<CustomerModel> GetCustomerInfo(CustomerLookupModel request, ServerCallContext context)
        {
            CustomerModel output = new CustomerModel();

            if (request.UserId == 1)
            {
                output.FirstName = "Jimmie";
                output.LastName = "Schimdt";
            }
            else if (request.UserId == 2)
            {
                output.FirstName = "Jane";
                output.LastName = "Doe";
            }
            else
            {
                output.FirstName = "Greg";
                output.LastName = "Tomas";
            }

            return Task.FromResult(output);

        }

        public async Task GetNewCustomers(
            NewCustomerRequest request, 
            IServerStreamWriter<CustomerModel> responseStream, 
            ServerCallContext context)
        {
            List<CustomerModel> customers = new List<CustomerModel>
            {
                new CustomerModel
                {
                    FirstName="Tim",
                    LastName="Corey",
                    EmailAddress="tim@gmail.com",
                    Age=41,
                    IsAlive=true
                },
                 new CustomerModel
                {
                    FirstName="Cloe",
                    LastName="Storm",
                    EmailAddress="cloestorm@gmail.com",
                    Age=26,
                    IsAlive=true
                 },
                  new CustomerModel
                {
                    FirstName="Billy",
                    LastName="Biblo",
                    EmailAddress="bilibiblo@gmail.com",
                    Age=113,
                    IsAlive=false
                    },
            };
            foreach(var cust in customers)
            {
                await responseStream.WriteAsync(cust);
            }
        }
    }

 
}


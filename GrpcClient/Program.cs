﻿using Grpc.Net.Client;
using GrpcServer;
using System;
using System.Threading.Tasks;
using GrpcClient;
using grpc = global::Grpc.Core;
namespace GrpcClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            //var input = new HelloRequest { Name = "Tim" };
            //var channel = GrpcChannel.ForAddress("https://localhost:5001");
            //var client = new Greeter.GreeterClient(channel);

            //var reply = await client.SayHelloAsync(input);

            //Console.WriteLine(reply.Message);

            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var customerClient = new Customer.CustomerClient(channel);

            var clientRequested = new CustomerLookupModel { UserId = 1 };

            var customer = await customerClient.GetCustomerInfoAsync(clientRequested);

           // Console.WriteLine($"{customer.FirstName}{customer.LastName}");
            Console.WriteLine();
            Console.WriteLine("Please entre the name of a customers: ");
            string name = Console.ReadLine();
            Console.WriteLine("The name is: " + name);
            Console.WriteLine();
                
            

            //using (var call = customerClient.GetNewCustomers(new NewCustomerRequest()))
            //{
            //    while (await call.ResponsesStream.MoveNext())
            //    {
            //        var currentCustomer = call.ResponsesStream.Current;

            //        Console.WriteLine($"{customer.FirstName} {customer.LastName}: {currentCustomer.EmailAddress } ");
            //    }
            //}

            Console.ReadLine();
        }
        
    }
}
